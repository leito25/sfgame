#include <tmbob/header/Bop.h>
#include <tmbob/header/TextureHolder.h>

Bop::Bop()
{
	//sprite que representa a Tom
	//--sprite traido de la clase character pero sobreescrito - override
	m_Sprite = Sprite(TextureHolder::GetTexture(
		"content/graphics/Bop.png"
	));

	m_JumpDuration = .25;//bop es más pesado
}

//definici´n de las funciones virtuales
bool Bop::handleInput()
{
	//inicia sin saltar
	m_JustJumped = false;

	if(Keyboard::isKeyPressed(Keyboard::Up))
	{
		//comenzar el salto su todavia no esta saltando
		//solo si esta quieto y tocando algun bloque
		if(!m_IsJumping && !m_IsFalling)
		{
			m_IsJumping = true;//activar el salto
			m_TimeThisJump = 0;//salto comienza en zero
			m_JustJumped = true;
		}
	}else{
		m_IsJumping = false;//si se acabo de presionar w entonces
		m_IsFalling = true;//el player esta cayendo pero ya no esta saltando
	}

	if(Keyboard::isKeyPressed(Keyboard::Left))
	{
		m_LeftPressed = true;
	}else{
		m_LeftPressed = false;
	}

	if(Keyboard::isKeyPressed(Keyboard::Right))
	{
		m_RightPressed = true;
	}else{
		m_RightPressed = false;
	}

	return m_JustJumped;//retorna el estado del salto


}