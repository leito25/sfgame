#include "tmbob/header/Engine.h"

void Engine::draw()
{
	//rub out the last frame
	m_Window.clear(Color::White);

	if(!m_SplitScreen)
	{
		//switch to bg view
		m_Window.setView(m_BGMainView);

		//draw the bg
		m_Window.draw(m_BackgroundSprite);

		//switch to mainview
		m_Window.setView(m_MainView);
	}
	else
	{
		//split screen view is active

		//TOMAS SIDE
		//dibujar el lado de thomas
		m_Window.setView(m_BGLeftView);
		//dibujar el fondo
		m_Window.draw(m_BackgroundSprite);
		//switch al left view
		m_Window.setView(m_LeftView);

		//BOB SIDE
		//dibujar el lado de thomas
		m_Window.setView(m_BGRightView);
		//dibujar el fondo
		m_Window.draw(m_BackgroundSprite);
		//switch al left view
		m_Window.setView(m_RightView);

	}

	//drawing the hud
	m_Window.setView(m_HudView);

	//show all we drawn
	m_Window.display();

}