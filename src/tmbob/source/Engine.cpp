#include <tmbob/header/Engine.h>

Engine::Engine()
{
	//tomar la resolución del compu
	Vector2f resolution;
	resolution.x = VideoMode::getDesktopMode().width;
	resolution.y = VideoMode::getDesktopMode().height;

	m_Window.create(VideoMode(resolution.x, resolution.y),
	"Tsl Game",
	Style::Fullscreen);

	//inicializar en full screen
	m_MainView.setSize(resolution);
	m_HudView.reset(
		FloatRect(0, 0, resolution.x, resolution.y)
	);


	//inicializar las vistar partidas
	m_LeftView.setViewport(
		FloatRect(0.001f, 0.001f, 0.498f, 0.998f)
	);
	//inicializar las vistar partidas
	m_RightView.setViewport(
		FloatRect(0.5f, 0.001f, 0.498f, 0.998f)
	);

	//inicializar las vistar partidas
	m_BGLeftView.setViewport(
		FloatRect(0.001f, 0.001f, 0.498f, 0.998f)
	);
	//inicializar las vistar partidas
	m_BGRightView.setViewport(
		FloatRect(0.5f, 0.001f, 0.498f, 0.998f)
	);


	m_BackgroundTexture = TextureHolder::GetTexture(
		"content/images/background.png"/*"../assets/images/background.png"*/
	);
	//ligar la textura con el sprite
	m_BackgroundSprite.setTexture(m_BackgroundTexture);

}

void Engine::run()
{
	//Timming
	Clock clock;

	while (m_Window.isOpen())
	{
		Time dt = clock.restart();
		//update total game time
		m_GameTimeTotal += dt;
		//Make a decimal fraction
		float dtAsSeconds = dt.asSeconds();


		//call each part of the game loop por turnos
		input();
		update(dtAsSeconds);
		draw();
	}

}