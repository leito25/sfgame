#include "tmbob/header/Engine.h"
#include <SFML/Graphics.hpp>
#include <sstream>

using namespace sf;

void Engine::update(float dtAssSeconds)
{
	if(m_Playing)
	{
		//contar cuando se deja al player
		m_TimeRemaining -= dtAssSeconds;

		//tom and bob run out time
		if(m_TimeRemaining <= 0)
		{
			m_NewLevelRequired = true;
		}
	}// End of playing
}