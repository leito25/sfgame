#include "tmbob/header/Engine.h"

void Engine::input()
{
	Event event;

	while(m_Window.pollEvent(event))
	{
		if(event.type == Event::KeyPressed)
		{
			//Dalir de la app
			if(Keyboard::isKeyPressed(Keyboard::Escape))
			{
				m_Window.close();
			}

			//Manejar el inició del juego
			if(Keyboard::isKeyPressed(Keyboard::Return))
			{
				m_Playing = true;
			}

			//cambiar entre thomasy bob
			if(Keyboard::isKeyPressed(Keyboard::Q))
			{
				m_Character1 = !m_Character1;
			}

			//switch entre full y split screen
			if (Keyboard::isKeyPressed(Keyboard::E))
			{
				m_SplitScreen = !m_SplitScreen;
			}
		}
	}

	//handle input for tom
	if(m_Tom.handleInput())
	{
		//play and jump sound
	}
	//handle input for bop
	if(m_Bop.handleInput())
	{
		//play and jump sound
	}
}