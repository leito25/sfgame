#include <tmbob/header/TextureHolder.h>
#include <assert.h>

using namespace sf;
using namespace std;

TextureHolder* TextureHolder::m_s_Instance = nullptr;

TextureHolder::TextureHolder()
{
	assert(m_s_Instance == nullptr);
	m_s_Instance = this;
}

sf::Texture& TextureHolder::GetTexture(std::string const& filename)
{
	//tomar las referencias a m_textures usando la m_s_Instance
	auto& m = m_s_Instance->m_Textures;
	//auto es igual a map<string, texture> pero
	//es mñas facil de usar solo auto&

	//se crea un iterador que sirve para buscar el
	//kvp - key-value-pair
	auto KeyValuePair = m.find(filename);
	//auto is like map<string, texture>::iterator


	//si se encuentra una de las parejas
	if (KeyValuePair != m.end())
	{
		//retornar la textura
		return KeyValuePair->second;//second is the texture
	}
	else{
		//sino crear un par nuevo
		auto& texture = m[filename];
		//cargar la textura
		texture.loadFromFile(filename);

		//retornar la textura
		return texture;
	}
}