#include <tmbob/header/Tom.h>
#include <tmbob/header/TextureHolder.h>

Tom::Tom()
{
	//sprite que representa a Tom
	//--sprite traido de la clase character pero sobreescrito - override
	m_Sprite = Sprite(TextureHolder::GetTexture(
		"content/graphics/Tom.png"
	));

	m_JumpDuration = .45;
}

//definici´n de las funciones virtuales
bool Tom::handleInput()
{
	//inicia sin saltar
	m_JustJumped = false;

	if(Keyboard::isKeyPressed(Keyboard::W))
	{
		//comenzar el salto su todavia no esta saltando
		//solo si esta quieto y tocando algun bloque
		if(!m_IsJumping && !m_IsFalling)
		{
			m_IsJumping = true;//activar el salto
			m_TimeThisJump = 0;//salto comienza en zero
			m_JustJumped = true;
		}
	}else{
		m_IsJumping = false;//si se acabo de presionar w entonces
		m_IsFalling = true;//el player esta cayendo pero ya no esta saltando
	}

	if(Keyboard::isKeyPressed(Keyboard::A))
	{
		m_LeftPressed = true;
	}else{
		m_LeftPressed = false;
	}

	if(Keyboard::isKeyPressed(Keyboard::D))
	{
		m_RightPressed = true;
	}else{
		m_RightPressed = false;
	}

	return m_JustJumped;//retorna el estado del salto


}