#pragma once
#include <SFML/Graphics.hpp>

using namespace sf;

class PlayableCharacter
{
	//***********PROTECTED**********
	//accesible just for childs
	protected:
	//the sprite
	Sprite m_Sprite;

	//characterirtis

	//how long jump last
	float m_JumpDuration;//jump time avalible char for

	//is jumping or falling
	bool m_IsJumping;
	bool m_IsFalling;

	//direction
	bool m_LeftPressed;
	bool m_RightPressed;

	//how logn jump - time
	float m_TimeThisJump;

	//initiate jump
	bool m_JustJumped = false;

	//*******************************
	private:
	//gravity
	float m_Gravity;

	//character spped
	float m_Speed = 400;

	//where is the player
	Vector2f m_Position;

	//characcter bodyparts
	FloatRect m_Feet;
	FloatRect m_Head;
	FloatRect m_Right;
	FloatRect m_Left;

	//the texture
	Texture m_Texture;

	//*******************************
	//then public
	public:

	void spawn(Vector2f startPosition, float gravity);

	//Esta es uan función virtual pura
	bool virtual handleInput() = 0;
	//ahora esta clase será abstracta y no podrá ser instanciada

	//ubicación del player
	FloatRect getPosition();

	//El sprite tiene diferentes partes para poder simular las colisiones
	//aqui se representan esos puntos
	FloatRect getFeet();
	FloatRect getHead();
	FloatRect getRight();
	FloatRect getLeft();

	//enviar una copia del sprite al main
	Sprite getSprite();

	//hacer que el personaje este firme o recto
	void stopFalling(float position);
	void stopRight(float position);
	void stopLeft(float position);
	void stopJump();

	//centro de character
	Vector2f getCenter();

	//the update fucntion every frame like unity
	void update(float elapseTime);

};