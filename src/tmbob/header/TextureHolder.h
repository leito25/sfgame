#pragma once
#ifndef TEXTURE_HOLDER_H
#define TEXTURE_HOLDER_H

#include <SFML/Graphics.hpp>
#include <map>

class TextureHolder
{
	private:
	//Un contenedor de mapa
	//es como un diccionarion con pares <nombre><textura>
	std::map<std::string, sf::Texture> m_Textures;

	//Un puntero para la clase Textureholder
	//que al ser estatica no se necesita instanciar
	static TextureHolder* m_s_Instance;

	public:
	TextureHolder();//default constructor

	//un metodo para traer el contenido de la imagen
	//segun el nombre
	//GetTexture return a reference to sf::texture type
	//se crea una referencia constante para el filename -const&
	static sf::Texture& GetTexture(std::string const& filename);//referencia constante
};

#endif