#pragma once
#include "PlayableCharacter.h"

class Tom : public PlayableCharacter
{
	public:
	//constructor de tom
	Tom();

	//overriden input - sobre escritura del input
	//se reemplaza el comportamiento
	//overload cambia solo los parametros
	bool virtual handleInput();


};