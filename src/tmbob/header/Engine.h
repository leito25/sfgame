#pragma once
#include <SFML/Graphics.hpp>
#include "TextureHolder.h"
//adding los characteres
#include <tmbob/header/Tom.h>
#include <tmbob/header/Bop.h>

using namespace sf;

class Engine
{
	private:
	//texture holder
	TextureHolder th;

	//instancing Tom and Bop
	Tom m_Tom;
	Bop m_Bop;

	const int TILE_SIZE = 50;
	const int VERT_IN_QUAD = 4;

	//gravity
	const int GRAVITY = 300;

	//render window
	RenderWindow m_Window;

	//el juego tiene varias vistas que se mezclan
	//Las vistas base
	View m_MainView;
	View m_LeftView;
	View m_RightView;
	//views for the background
	View m_BGMainView;
	View m_BGLeftView;
	View m_BGRightView;
	//hud
	View m_HudView;


	//Declare de sprite and texture para el background
	Sprite m_BackgroundSprite;
	Texture m_BackgroundTexture;

	//game state -- currentplaying?
	bool m_Playing = false;

	//Is character 1 or 2 el que se esta utilizando
	bool m_Character1 = true;

	//split screen
	bool m_SplitScreen = false;

	//time left
	float m_TimeRemaining = 10;
	Time m_GameTimeTotal;

	//new/first level
	bool m_NewLevelRequired = true;

	//Private function (just internal)
	void input();
	void update(float dtAsSeconds);
	void draw();

	public:
	//the engine constructor
	Engine();

	//run es la unica publica y activa las demás que estñan privadas
	void run();


};