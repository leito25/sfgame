#include "tmbob/header/Engine.h"

int main()
{
	//Instancia del engine
	Engine myEngine;

	//start the engine
	myEngine.run();

	//stop the app
	return 0;
}