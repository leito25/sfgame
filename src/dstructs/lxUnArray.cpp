#include <iostream>
using namespace std;

#include<stdlib.h>
#include<stdio.h>

template<class T>// crea una clase tipo T

class lxUnArray
{
	public:
	lxUnArray(int size, int growBy = 1) ://arg tamaño y un valor de crecimiento
		m_array(NULL), m_maxSize(0),//array en null - maxsize 0
		m_growSize(0), m_numElements(0)//crecimiento y numeroElementos
		{
			if(size)//si existe un size - tamaño
			{
				m_maxSize = size;//el tamaño del arreglo
				m_array = new T[m_maxSize];//se construye el array en memoria

				m_growSize = ((growBy > 0) ? growBy : 0);//asignando el growsize
			}
		}

	virtual ~lxUnArray()//destructor
	{
		if(m_array != NULL)//si no esta vacio o null
		{
			delete[] m_array;//borrar el array de memoria
			m_array = NULL;//poner el arreglo en null
		}
	}

	//INSERT FUNCTION
	virtual void push(T val)
	{
		//assert checkea si existe o no el puntero en memoria
		assert(m_array != NULL);//SOLO PARA EL DEBUGGING - JUST FOR DEBUGGING

		//si al momento de agregan un item aumenta el tamño del array
		//se utiliza una funxión Expand que crea un nuevo array con el size adecuado
		//at the moment to insert a new item the expand function going to create
		//a new array with the according size, like a copy.
		if(m_numElements >= m_maxSize)
		{
			Expand();
		}

		m_array[m_numElements] = val;//asigna el valor al ultimo item
		m_numElements++;//actualizar el numero de elementos a 1 más
	}

	//POP TO REMOVE THE LAST ITEM - PARA REMOVER EL ULTIMO ITEM
	void pop()
	{
		if(m_numElements > 0)
		m_numElements--;
	}

	// TO REMOVE FROM A SPECIFIC INDEX - REMOVER DESDE UN INIDICE ESPECIFICO
	void remove(int index)
	{
		assert(m_array != NULL);//check the array is not empty

		//check if the item position is into the array
		if(index >= m_maxSize)
		{
			return;//si es mayor sale de la función ya que no existe
		}

		for (int k = index; k < m_maxSize - 1; k++)
		{
			//quita un valor y corre todos lo de esa posición
			//hacia abajo para arriba
			m_array[k] = m_array[k+1];
		}

		m_maxSize--;//quitar una unitad del total

		//igualando num elements y el maxsize
		if(m_numElements >= m_maxSize)
		m_numElements = m_maxSize - 1;
	}

	//overload operator
	//función similar a un array tradicional de direccionar
	//por mediod eun indice un valor  array[x] //x = index
	virtual T& operator[](int index)
	{
		//si no esta vacio o el index esta pro fuera del arreglo
		assert(m_array != NULL && index <= m_numElements);
		return m_array[index];
	}

	//linear search in a unordered array
	virtual int linear_search(T val)
	{
		assert(m_array != NULL);

		for(int i = 0; i < m_numElements; i++)
		{
			if (m_array[i] == val)
				return i;
		}

		return -1;
	}

	void clear() { m_numElements = 0; } //limpiar el array
	int GetSize() { return m_numElements; } //getter m_numElements
	int GetMaxSize() { return m_maxSize; } //getter
	int GetGrowSize() { return m_growSize; } //getter

	void SetGrowSize(int val)// setter el growing by
	{
		assert(val >= 0);
		m_growSize = val;
	}

	//---------------------------
	//BUBBLE SORT
	void BubbleSort()
	{
		//valores de comparaciones
		//valores de intercambios
		int comp = 0;
		int inter = 0;

		assert(m_array != NULL);//checkear que exista allocation

		//valor temporal
		T temp;

		//se hace un recorrido de adelante hacia atras
		for(int k = m_numElements - 1; k > 0; k--)
		{
			//recorrido de atras a adelante
			for(int i = 0; i < k; i++)
			{
				//comparaciones
				comp++;
				//sort con la ayuda de un temporal
				if (m_array[i] > m_array[i+1])
				{
					temp = m_array[i];
					m_array[i] = m_array[i+1];
					m_array[i+1] = temp;
					inter++;
				}
			}
		}

		cout << "Comparaciones Bubble Sort : " << comp << endl;
		cout << "Intercambios Bubble Sort : " << inter << endl;
	}

	///----------------------------------
	//SELECTION SORT
	void SelectionSort()
	{
		assert(m_array != NULL);

		T temp;
		int min = 0;

		for (int k = 0; k < m_numElements - 1; k++)
		{
			min = k;

			for(int i = k+1; i < m_numElements; i++)
			{
				if(m_array[i] < m_array[min])
				{
					min = i;
				}
			}

			if (m_array[k] > m_array[min])
			{
				temp = m_array[k];
				m_array[k] = m_array[min];
				m_array[min] = temp;
			}
		}
	}

	///----------------------------------
	//INSERTION SORT
	void InsertionSort()//la publica
	{
		assert(m_array != NULL);//verificar la nullidad
		//temp value
		T temp;
		int i = 0;
		//contador para escoger el valor
		//en cada iteración

		for (int k = 1; k < m_numElements; k++)
		//k en uno para comparar con el anterior
		{
			temp = m_array[k];
			i = k;

			while(i > 0 && m_array[i - 1] >= temp)//si es mayor que el temporal
			{
				m_array[i] = m_array[i - 1];
				i--;
			}

			m_array[i] = temp;//insertion
		}
	}

	//-------merge sort publico
	void MergeSort()//entry point
	{
		assert(m_array != NULL);//comprobar alloc

		//array extra temporal
		T *temp_Array = new T[m_numElements];
		assert(temp_Array != NULL);//comprob. alloc

		//paso de datos del array inicial al temporal
		for (int x = 0; x < m_numElements; x++)
		{
			temp_Array[x] = m_array[x];
		}

		//función recursiva
		//la recursiva me va a construir los arreglos partidos
		//le pasamos el arreglo temporal// el limite inf // lim superior
		MergeSort(temp_Array, 0, m_numElements - 1);// n-1
		delete[] temp_Array;//al final borrar el array temporal
	}

	//override de metodo MergeSort pero privado
	private:
	void MergeSort(T *temp_Array, int lowerBound, int upperBound)
	{
		if(lowerBound < upperBound)
		{
			//se crea el rango entre el lower y el upper y se le suma uno
			//int mid = (lowerBound + upperBound) >> 1;
			int mid = lowerBound + (upperBound-lowerBound) / 2;

			//ahora se hace el sort recursivo con cada uno
			//del lower a medio   - y del medio+1 al upper

			MergeSort(temp_Array, lowerBound, mid);
			MergeSort(temp_Array, mid + 1, upperBound);
			Merge(temp_Array, lowerBound, mid, upperBound);
		}
	}

	private:
	void Merge(T *temp__Array, int l, int m, int r)
	{
		//
		int i, j, k;
		int n1 = m - l + 1;
		int n2 = r - m;

		//crear los arrays temporales
		int* tempArr_L = new int[n1];
		int* tempArr_R = new int[n2];

		//copiar lso datos a cada array
		//array izquiero de low a la mitad
		//array derecho de mitad+1 a upper
		for(i = 0; i < n1; i++)
		{
			tempArr_L[i] = temp__Array[l + i];
			//cout << "::" << tempArr_L[i] << endl;
			//cout << "::" << temp__Array[l + i] << endl;
		}
		for(j = 0; j < n2; j++)
		{
			tempArr_R[j] = temp__Array[m+1+j];
			//cout << "::" << tempArr_R[i] << endl;
			//cout << "::" << temp__Array[m+1+j] << endl;
		}

		//al final se hace el merge en el array principal
		i = 0;//initial index subarray 1 -izq
		j = 0;//initial index subarray 2 -der
		k = l;//Initial index de array mezclado -que es el low


		while (i < n1 && j < n2)
		{
			/* code */
			if (tempArr_L[i] <= tempArr_R[j])
			{
				temp__Array[k] = tempArr_L[i];
				i++;
			}
			else
			{
				temp__Array[k] = tempArr_R[j];
				j++;
			}
			k++;
		}

		//los elementos de L
		while (i < n1)
		{
			temp__Array[k] = tempArr_L[i];
			//cout << temp__Array[k];
			i++;
			k++;
		}

		//los elementos de R
		while (j < n2)
		{
			temp__Array[k] = tempArr_R[j];
			//cout << temp__Array[k];
			j++;
			k++;
		}

		//copia qde datos al array principal
		for (int x = 0; x < m_numElements; x++)
		{
			m_array[x] = temp__Array[x];
		}

	}



	public:
	void print()
	{
		cout << "Printed Array" << endl;
		for (int i = 0; i < m_numElements; i++)
		{
			//print
			cout << "index[" << i << "]: " << m_array[i] << endl;
		}
	}

	void fillrandomly(int range_limit)
	{
		srand(time(NULL));
		for (int i = 0; i < m_numElements; i++)
		{
			T random_number = rand() % range_limit;
			m_array[i] = random_number;
		}
	}

	private:

	//exapndir el arreglo copiandolo en otro con los valores nuevos
	//expand the array copying all the data in other and the used as amin array
	bool Expand()
	{
		if(m_growSize <= 0)
		return false;

		T *temp = new T[m_maxSize + m_growSize]; //el tamaño del array + el grow size
		assert(temp != NULL);

		memcpy(temp, m_array, sizeof(T) * m_maxSize);

		delete[] m_array;
		m_array = temp;

		m_maxSize += m_growSize;

		return true;
	}


	private:
	T *m_array;

	int m_maxSize;
	int m_growSize;
	int m_numElements;
};