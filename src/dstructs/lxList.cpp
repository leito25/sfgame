template<typename T> class LinkIterator;//clase iterador
template<typename T> class LinkList;//clase lista enlazada

template<typename T>
class LinkNode//clase del nodo
{
	friend class LinkIterator<T>;
	friend class LinkList<T>;

	private:
	T m_data;
	LinkNode *m_next;
};

//La clase iterador lo que hace
//es sebreescribir un poco los operadores tradicionales
//de las listas stl para las operaciones básicas
template<typename T>
class LinkIterator
{
	public:
	LinkIterator()
	{
		m_node = nullptr;//null cuando lo contruyo
	}

	~LinkIterator()
	{
		//
	}

	//operador  =
	//asigna el valor que contiene el nodo
	void operator = (LinkNode<T> *node)
	{
		m_node = node;
	}

	//operador de consulta ()
	//devuelve el valor del nodo que se solicita en los parentesis
	T &operator*()
	{
		assert(m_node != nullptr);
		return m_node->m_data;
	}

	//operador ++
	void operator++(int)//toma el valor del nodo siguiente
	{
		assert(m_node != nullptr);

		m_node = m_node->m_next;
	}

	//operador para chequear desigualdad
	bool operator!=(LinkNode<T> *node)
	{
		return (m_node != node);
	}

	//operador para chequear desigualdad
	bool operator==(LinkNode<T> *node)
	{
		return (m_node == node);
	}

	private:
	LinkNode<T> *m_node;
};

//linklist based in the stl class
template<typename T>
class LinkList
{
	public:
	LinkList():m_size(0), m_root(0), m_lastNode(0)
	{
		//constructor que inicializa todos los valores
	}

	~LinkList()
	{
		while(m_root != nullptr)//destruir todos los nodos
		{
			Pop();
		}
	}

	//marca el begin del iterador --> retorna el root
	LinkNode<T> *Begin()
	{
		assert(m_root != nullptr);
		return m_root;
	}

	//Marca el ultimo nodo, en principio NULL
	LinkNode<T> *End()
	{
		return nullptr;
	}

	void Push(T newData)//funcion de agregar
	{
		//se crea un nuevo nodo
		LinkNode<T> *node = new LinkNode<T>;

		//se verifica que no sea null
		assert(node != nullptr);

		//se asigna el valor
		node->m_data = newData;
		//se asigan el puntero al nodo anterios
		node->m_next = nullptr;

		//si el ultimo nodo no es null
		//se le asigna el nodo como last node
		if(m_lastNode != nullptr)
		{
			//
			m_lastNode->m_next = node;
			m_lastNode = node;
		}
		else
		{
			//si el ultimo es null
			//el nodo sera el primero y el ultimo
			m_root = node;
			m_lastNode = node;
		}

		m_size++;//incrementa en uno la lista

	}

	//POP --> estallar el ultimo item de la lista
	void Pop()
	{
		assert(m_root != nullptr);//verificar su existe el root

		//si solo hay un nodo, el nodo next esta null
		//entonces se borra el root y se deja como null
		if(m_root->m_next == nullptr)
		{
			delete m_root;
			m_root = nullptr;
		}
		else
		{
			//se crea la referencia al un nodo anterior
			//que sera el root
			LinkNode<T> *prevNode = m_root;

			//si el nodo no es ni el primero ni el ultimo
			//solo se enlaza al nodo creado
			while(prevNode->m_next != NULL && prevNode->m_next != m_lastNode)
			{
				prevNode = prevNode->m_next;
			}

			delete m_lastNode;
			prevNode->m_next = NULL;
			m_lastNode = prevNode;

			//organización de los nodos
		}

		m_size = (m_size == 0 ? m_size:m_size-1);

	}

	int GetSize()
	{
		return m_size;
	}


	private:
	int m_size;
	LinkNode<T> *m_root;//nodo root - primer nodo
	LinkNode<T> *m_lastNode;//ultimo nodo
};